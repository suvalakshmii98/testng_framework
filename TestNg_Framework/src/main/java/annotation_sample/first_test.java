package annotation_sample;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;

public class first_test {

	// if more than one number of test methods are there, testng will run in
	// alphabetical order//
	@org.testng.annotations.Ignore
	@Test(priority = 1,invocationCount = 3)
	public void TestGoogle() throws Exception {
		System.setProperty("webdriver.chrome.driver", "D:\\TESTINGCOURSE\\drivers\\chromedriver110.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.google.com/");
		driver.findElement(By.name("q")).sendKeys("cheese", Keys.ENTER);
		// assertion - two types - 1. Hard Assert 2/Soft Assert
		
		//hard assertion
		String expectedtitle = "cheese - Google Search";
		String actualtitle = driver.getTitle();
		Assert.assertEquals(actualtitle, expectedtitle,"title validation");// error 
		//Assert.assertEquals(actualtitle, expectedtitle, "title is mismatch");
		
		Thread.sleep(3000);
		driver.quit();
	}

	
	@Test(priority = 2)
	public void TestFacebook() throws Exception {
		System.setProperty("webdriver.chrome.driver", "D:\\TESTINGCOURSE\\drivers\\chromedriver110.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.facebook.com/");
		driver.findElement(By.name("email")).sendKeys("kowsaljdaskahfl", Keys.ENTER);
		Thread.sleep(5000);

		SoftAssert softassert = new SoftAssert();
		//soft assert will throw and expection at end of the code when any assert failed, and it will execute the line afer the error as well.
		//softassert.assertall() - is used to throw the error after code running
		
		// Title assertion - soft assert example
		String actualtitle = driver.getTitle();
		String expectedtitle = "Log in to Facebookvgbfgn";
		softassert.assertEquals(actualtitle, expectedtitle, "title is mismatch"); //error

		// URL Assertion
		String actualurl = driver.getCurrentUrl();
		String expectedurl = "https://www.facebook.com/";
		softassert.assertNotEquals(actualurl, expectedurl, "URL is mismatch");

		// Text Assertion
		String actualtext = driver.findElement(By.name("email")).getAttribute("value");
		String expectedtext = "";
		softassert.assertEquals(actualtext, expectedtext, "Username Text is mismatch");

		// Border Assertion
		String actualborder = driver.findElement(By.name("email")).getCssValue("border");
		String expectedborder = "0.8px solid rgb(240, 40, 73)";
		softassert.assertEquals(actualborder, expectedborder, "Border is mismatch");
		
		//error message
		String actualerrormsg = driver.findElement(By.xpath("//div[@id='email_container']//div[2]")).getText();
		String expectederrormsg = "The email address or mobile number you entered isn't connected to an account. Find your account and log in.";
		softassert.assertEquals(actualerrormsg, expectederrormsg, "error message is mismatch");

		System.out.println(driver.getTitle());
		driver.quit();
		softassert.assertAll();

	}
}
