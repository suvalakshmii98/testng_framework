package annotation_sample;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Sample_annotation {
	@Test
	  public void testmethod1() {
		  System.out.println("Test method 1");

		 
	}
	//
	//
	//
	  @Test
	  public void testmethod2() {
		  System.out.println("Test method 2");
	  }
	  @BeforeMethod
	  public void beforeMethod() {
		  System.out.println("beforeMethod");

	  }

	  @AfterMethod
	  public void afterMethod() {
		  System.out.println("afterMethod");

	  }

	  @BeforeClass
	  public void beforeClass() {
		  System.out.println("beforeClass");
	  }

	  @AfterClass
	  public void afterClass() {
		  System.out.println("afterClass");
	  }

	  @BeforeTest
	  public void beforeTest() {
		  System.out.println("beforeTest");
	  }

	  @AfterTest
	  public void afterTest() {
		  System.out.println("afterTest");
	  }
	  

	  @BeforeSuite
	  public void beforeSuite() {
		  System.out.println("beforeSuite");
	  }

	  @AfterSuite
	  public void afterSuite() {
		  System.out.println("afterSuite");
	  }
}
